import csv, sys, os
import django

#pathend('C:\Users\Andrey\Documents\My webApps\')

os.environ["DJANGO_SETTINGS_MODULE"]="numTests.settings"

django.setup()

from verbal.models import Verbalquest, Verbaltest

data = csv.reader(open("C:/Users/Andrey/Documents/My webApps/numTests/verbal2.csv"), delimiter=";")

verbal1 = Verbaltest()

verbal1.testName = "Вариант 2"
verbal1.testCoverageStatistics = "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1"
verbal1.save()

# for row in data:
#     print(row[0])
verbal2 = Verbaltest.objects.get(pk=2)

for row in data:
    quests = Verbalquest()
    quests.verbalTest = verbal2
    quests.numInRow = int(row[0])
    quests.question = row[1]
    quests.question_txt = row[2]
    quests.rightAnswer = row[3]
    quests.rightAnswerNum = int (row[4])
    quests.answerOne = row[5]
    quests.answerTwo = row[6]
    quests.answerThree = row[7]
    
    
    quests.save()

print(verbal2.verbalquest_set.all())