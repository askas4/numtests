import csv, sys, os
import django

#pathend('C:\Users\Andrey\Documents\My webApps\')

os.environ["DJANGO_SETTINGS_MODULE"]="numTests.settings"

django.setup()

from tqquestions.models import TQquest, TQtest

data = csv.reader(open("C:/Users/Andrey/Documents/My webApps/numTests/qTq1.csv"), delimiter=";")

tqTest1 = TQtest()

tqTest1.testName = "Вариант 1"
tqTest1.testCoverageStatistics = "1,1,1,1,1,1,1,1,1,1"
tqTest1.save()

# for row in data:
#     print(row[0])
tqTest2 = TQtest.objects.get(pk=3)

for row in data:
    tqquests = TQquest()
    tqquests.tqTest = tqTest2
    tqquests.numInRow = int(row[0])
    tqquests.question = unicode(row[1], "utf-8")
    tqquests.question_img = row[2]
    tqquests.rightAnswer = row[3]
    tqquests.rightAnswerNum = int (row[4])
    tqquests.answerOne = row[5]
    tqquests.answerTwo = row[6]
    tqquests.answerThree = row[7]
    tqquests.answerFour = row[8]
    tqquests.answerFive = row[9]
    tqquests.answerSix = row[10]
    tqquests.answerSeven = row[11]
    tqquests.answerEight = row[12]
    tqquests.answerNine = row[13]
    tqquests.answerTen = row[14]
    tqquests.save()

print(tqTest2.tqquest_set.all())