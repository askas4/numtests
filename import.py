import csv, sys, os
import django

#pathend('C:\Users\Andrey\Documents\My webApps\')

os.environ["DJANGO_SETTINGS_MODULE"]="numTests.settings"

django.setup()

from shlquestions.models import Shlquest, Shltest

data = csv.reader(open("C:/Users/Andrey/Documents/My webApps/numTests/вопросы.csv"), delimiter=";")

shlTest1 = Shltest.objects.get(pk=1)

# for row in data:
#     print(row[0])


for row in data:
    shlquest = Shlquest()
    shlquest.shlTest = shlTest1
    shlquest.numInRow = int(row[0])
    shlquest.question = row[1]
    shlquest.question_img = row[2]
    shlquest.rightAnswer = row[3]
    shlquest.rightAnswerNum = int (row[4])
    shlquest.answerOne = row[5]
    shlquest.answerTwo = row[6]
    shlquest.answerThree = row[7]
    shlquest.answerFour = row[8]
    shlquest.save()

print(shlTest1.shlquest_set.all())