from django.conf.urls import url
from . import views

app_name = 'home'

urlpatterns = [
   
    url(r'^$', views.home, name='home'),
    url(r'^shl-testy/$', views.shl, name='shl'),
    url(r'^testy-talent-q/$', views.talentq, name='talent-q'),
    url(r'^ontarget-testy/$', views.ontarget, name='ontarget'),
    url(r'^kenexa/$', views.kenexa, name='kenexa'),
    url(r'^saville-assessment/$', views.saville, name='saville-assessment'),
    url(r'^test-uotsona-gleyzera/$', views.watsonglaser, name='watson-glaser'),
    url(r'^robots\.txt$', views.robotsTxt),
    url(r'^ads\.txt$', views.adsTxt),
]

