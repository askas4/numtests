from django.conf.urls import url
from . import views

app_name = 'contact'

urlpatterns = [
    #/contact/
    url(r'^$', views.contact, name='contact'),
    #/contact/sendMsg/
    url(r'^sendMsg/$', views.sendMsg),  
]

