from django import forms

class ContactForm(forms.Form):
    name = forms.CharField(label = "Имя *")
    email = forms.EmailField(label = "E-mail *")
    category = forms.ChoiceField(choices=[('quest', 'Question'),('othe', 'Others')], label = "Тип запроса (выберите) *")
    subject = forms.CharField(required = False)
    body = forms.CharField(widget = forms.Textarea)