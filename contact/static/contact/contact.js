jQuery('document').ready(function(){
            
          // изменение цвета иконок верхнего меню при наведении
      
     

     // добавление элементов в выпадающее адаптивное меню



var labelID;
$('.wrapper label').click(function() {
       labelID = $(this).attr('for');
       $('#'+labelID).prop( "checked", true ); 
});

$('.g-recaptcha').click(function() {
  $('.recaptcha1 p').css('display', 'none');
});

// FORM VALIDATION
 
  var name_valid = false;
  var email_valid = false;
  var category_valid = false;
  var subject_valid = false;
  var message_valid = false;
  var finalValidation = false;
  var captcha = false;
   
  function  textAndNumberValidation(input){
    if (input.value.match(/[^a-zA-zА-я0-9\s]/g)){
        $('#name p:eq(1)').css('display', 'inline-flex');
        $('.feedback label:eq(0)').css('color', 'red');
      name_valid = false;
    } else{
      name_valid = true;
       $('#name p:eq(1)').css('display', 'none');
       $('#name p:eq(0)').css('display', 'none');
       if(input.value == ""){
        $('.feedback label:eq(0)').css('color', '#747479');

             if(finalValidation){
              $('#name p:eq(0)').css('display', 'inline-flex');
              $('.feedback label:eq(0)').css('color', 'red');
              name_valid = false;
             }

          }else{
         $('.feedback label:eq(0)').css('color', '#1E4495');
        }
      }
    }



function check_email(input){
    if(((!input.value.match(/\S+@\S+\.\S+/))||(input.value.indexOf(' ')!=-1 || input.value.indexOf('..')!=-1))&&(input.value != "")){ // Jaymon's / Squirtle's solution
      $('#email p:eq(1)').css('display', 'inline-flex');
      $('.feedback label:eq(1)').css('color', 'red');
      email_valid = false;
       if(finalValidation){
         $('#email p:eq(0)').css('display', 'none');
       }
        /*return false;*/
    } else{
      email_valid = true;
       $('#email p:eq(1)').css('display', 'none');
       $('#email p:eq(0)').css('display', 'none');
       if(input.value == ""){
        $('.feedback label:eq(1)').css('color', '#747479');
             
             if(finalValidation){
              $('#email p:eq(0)').css('display', 'inline-flex');
              $('.feedback label:eq(1)').css('color', 'red');
              email_valid = false;
             }

          }else{
         $('.feedback label:eq(1)').css('color', '#1E4495');
        }
      }
    }   
    
function check_category(input){
     if(input.value == ""){
        $('.feedback label:eq(2)').css('color', '#747479');
          category_valid = false;

            if(finalValidation){
              $('#category p:eq(0)').css('display', 'inline-flex');
              $('.feedback label:eq(2)').css('color', 'red');
              category_valid = false;
             }
          
          }else{
            category_valid = true;
         $('.feedback label:eq(2)').css('color', '#1E4495');
         $('#category p:eq(0)').css('display', 'none');
        }
      }
       
  function check_subject(input){
    
     if (input.value.length>60){
        $('#subject p:eq(1)').css('display', 'inline-flex');
      $('.feedback label:eq(3)').css('color', 'red');
      subject_valid = false;
    } else{
       $('#subject p:eq(1)').css('display', 'none');
       $('#subject p:eq(0)').css('display', 'none');
       subject_valid = true;
       if(input.value == ""){
        $('.feedback label:eq(3)').css('color', '#747479');

          if(finalValidation){
              $('#subject p:eq(0)').css('display', 'inline-flex');
              $('.feedback label:eq(3)').css('color', 'red');
              subject_valid = false;
             }

          }else{
         $('.feedback label:eq(3)').css('color', '#1E4495');
        }
      }
    }

 function check_message(input){
     /*console.log(input.value.length);*/
     if (input.value.length>1500){
        $('#message p:eq(1)').css('display', 'inline-flex');
      $('.feedback label:eq(4)').css('color', 'red');
      message_valid = false;
    } else{
      message_valid = true;
       $('#message p:eq(1)').css('display', 'none');
       $('#message p:eq(0)').css('display', 'none');
       if(input.value == ""){
        $('.feedback label:eq(4)').css('color', '#747479');

        if(finalValidation){
              $('#message p:eq(0)').css('display', 'inline-flex');
              $('.feedback label:eq(4)').css('color', 'red');
              message_valid = false;
             }


          }else{
         $('.feedback label:eq(4)').css('color', '#1E4495');
        }
      }
    }

    

   var nameInput = document.getElementById("id_name");
     nameInput.addEventListener('keyup', function(){
     textAndNumberValidation(this);
   });

   var emailInput = document.getElementById("id_email");
     emailInput.addEventListener('keyup', function(){
     check_email(this);
   });

   var categoryInput = document.getElementById("id_category");
     categoryInput.addEventListener('change', function(){
     check_category(this);
   });

   var subjectInput = document.getElementById("id_subject");
     subjectInput.addEventListener('keyup', function(){
     check_subject(this);
   });

   var messageInput = document.getElementById("id_message");
     messageInput.addEventListener('keyup', function(){
     check_message(this);
   });


     var inputs = [];
     inputs.push(nameInput);
     inputs.push(emailInput);
     inputs.push(categoryInput);
     inputs.push(subjectInput);
     inputs.push(messageInput);

   for(i=0; i<inputs.length; i++){
  
    inputs[i].value = "";
}
// НАЖАТИЕ ОТПРАВИТЬ

   $('.send').click(function(){
   
   
    $('.loader').removeClass('hide');
     $('.send').addClass('hide');
    
   
   finalValidation = true;

   textAndNumberValidation(nameInput);
   check_email(emailInput);
   check_category(categoryInput);
   check_subject(subjectInput);
   check_message(messageInput); 
   
   var grecaptchaResponse = grecaptcha.getResponse();
   if(grecaptchaResponse.length == 0)
   {
    captcha = false;
    $('.recaptcha1 span').removeClass('hide');
    setTimeout(
      function (){
          $('.recaptcha1 span').addClass('hide');
       }, 
      2335);
   }else{
    captcha = true;
    $('.recaptcha1 span').addClass('hide');
   }
    

   for(i=0; i<1000; i++){
      z=1;

    }

   if(name_valid&&
      email_valid&&
      category_valid&&
      subject_valid&&
      message_valid&&
      finalValidation&&
      captcha
      ){
    
        $.ajax({
          type:'POST',
          url:'sendMsg/',
          dataType:'json',
          async: false,
          data: {
             csrfmiddlewaretoken: csrftoken,
             recaptchaResponse: grecaptchaResponse,
             name: nameInput.value,
             emai: emailInput.value,
             category: categoryInput.value,
             subject: subjectInput.value,
             message: messageInput.value,  
           }
           ,
           success: function(json){
               if(json.mailsuccess){
                $('.onSuccess').removeClass('hide');
                $('.customForm').addClass('hide');
               }else{
                   $('.respRes').removeClass('hide');
                   $('.send').removeClass('hide');
                   $('.loader').addClass('hide');
                   grecaptcha.reset();
                 }
             }
        });
 
      
   }else{
   
   
   $('.send').removeClass('hide');
   $('.loader').addClass('hide');
   
    }

 });








});