# Generated by Django 2.1.1 on 2018-10-15 19:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TQquest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numInRow', models.IntegerField()),
                ('question', models.CharField(max_length=1000)),
                ('question_img', models.FileField(blank=True, null=True, upload_to='')),
                ('rightAnswer', models.CharField(max_length=200)),
                ('rightAnswerNum', models.IntegerField()),
                ('answerOne', models.CharField(max_length=200)),
                ('answerTwo', models.CharField(max_length=200)),
                ('answerThree', models.CharField(max_length=200)),
                ('answerFour', models.CharField(max_length=200)),
                ('answerFive', models.CharField(max_length=200)),
                ('answerSix', models.CharField(max_length=200)),
                ('answerSeven', models.CharField(max_length=200)),
                ('answerEight', models.CharField(max_length=200)),
                ('answerNine', models.CharField(max_length=200)),
                ('answerTen', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='TQtest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('testName', models.CharField(max_length=250)),
                ('testCoverageStatistics', models.CharField(max_length=300)),
            ],
        ),
        migrations.AddField(
            model_name='tqquest',
            name='tqTest',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='tqquestions.TQtest'),
        ),
    ]
