from django.shortcuts import render, redirect
from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect, Http404
from tqquestions.models import TQquest, TQtest
import json
import random

def url_redirect(request):
    
   
    return redirect('/numerical/talent-q/1/')


def tqtest_view(request, tqtest_id):
    try:
        tqTest1 = TQtest.objects.get(pk=tqtest_id)
    except TQtest.DoesNotExist:
        raise Http404

    question = []
    image = []
    for i in range(1, tqTest1.tqquest_set.count()+1):
        quest1=tqTest1.tqquest_set.get(numInRow=i)
        question.append(quest1.question)
        image.append(str(quest1.question_img))
    qmlist =  zip(question, image)
    adverNum = str(random.randint(1,1))
    context = {
        'qmlist': qmlist,
        'tqtest_id': tqtest_id,
        'adverNum': adverNum,
       }
    return render(request, 'tqquestions/tq.html', context)

    #  return HttpResponse('<h1>we are in SHL '+str(shltest_id)+' test app</h1>')

def get_data(request, tqtest_id):
    if request.is_ajax():
        try:
            tqTest1 = TQtest.objects.get(pk=tqtest_id)
        except TQtest.DoesNotExist:
            raise Http404

        response_data = {}
        answers = []
        rightAnswers = []
        for i in range(1, tqTest1.tqquest_set.count()+1):
            quest1=tqTest1.tqquest_set.get(numInRow=i)
            temp = []
            temp.append(quest1.answerOne)
            temp.append(quest1.answerTwo)
            temp.append(quest1.answerThree)
            temp.append(quest1.answerFour)
            temp.append(quest1.answerFive)
            temp.append(quest1.answerSix)
            temp.append(quest1.answerSeven)
            temp.append(quest1.answerEight)
            temp.append(quest1.answerNine)
            temp.append(quest1.answerTen)
            answers.append(temp)
            rightAnswers.append(quest1.rightAnswerNum)
       
        x = tqTest1.testCoverageStatistics
        statistics = x.split(",")
        statistics = list(map(int, statistics))
        
        response_data['answers'] = answers
        response_data['rightAnswers'] = rightAnswers
        response_data['statistics'] = statistics
        return HttpResponse(json.dumps(response_data), content_type='application/json')
    else:
        raise Http404

def post_data(request, tqtest_id):
    if request.is_ajax():
        theNumber = int(request.POST.get("theNumber", ""))
        if (theNumber > 0):
            try:
                tqTest1 = TQtest.objects.get(pk=tqtest_id)
            except:
                 pass
            else:
                 statistics = []
                 x = tqTest1.testCoverageStatistics
                 statistics = x.split(",")
                 statistics = list(map(int, statistics))
                 statistics[theNumber-1] = statistics[theNumber-1]+1
                 newStatistics = ','.join(map(str, statistics)) 
                 tqTest1.testCoverageStatistics = newStatistics
                 tqTest1.save()
                 return HttpResponse('')
        else:
            return HttpResponse('')
                 # x = shlTest1.testCoverageStatistics
                 # response_data = {}
                 # response_data['someNumber'] = x
                 # return HttpResponse(json.dumps(response_data), content_type='application/json')