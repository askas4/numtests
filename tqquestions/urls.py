from django.conf.urls import url
from . import views
from .models import TQtest

app_name = 'tqquestions'

urlpatterns = [
    #/tqtest/
    url(r'^$', views.url_redirect, name='tqredirect'),
    #/tqltest/791/
    url(r'^(?P<tqtest_id>[0-9]+)/$', views.tqtest_view, name='tqtest_view'),
     #/tqltest/1/get_data/
    url(r'^(?P<tqtest_id>[0-9]+)/get_data/$', views.get_data),
     #/tqltest/1/post_data/
    url(r'^(?P<tqtest_id>[0-9]+)/post_data/$', views.post_data),
]

