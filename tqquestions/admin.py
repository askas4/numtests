from django.contrib import admin

# Register your models here.
from .models import TQquest, TQtest

admin.site.register(TQtest)
admin.site.register(TQquest)