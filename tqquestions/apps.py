from django.apps import AppConfig


class TqquestionsConfig(AppConfig):
    name = 'tqquestions'
