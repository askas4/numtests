import csv, sys, os
import django

#pathend('C:\Users\Andrey\Documents\My webApps\')

os.environ["DJANGO_SETTINGS_MODULE"]="numTests.settings"

django.setup()

from logic.models import Logicquest, Logictest

data = csv.reader(open("C:/Users/Andrey/Documents/My webApps/numTests/logic2.csv"), delimiter=";")

# logic1 = TQtest()

# tqTest1.testName = "Вариант 1"
# tqTest1.testCoverageStatistics = "1,1,1,1,1,1,1,1,1,1"
# tqTest1.save()

# for row in data:
#     print(row[0])
logic2 = Logictest.objects.get(pk=2)

for row in data:
    logicquests = Logicquest()
    logicquests.LogicTest = logic2
    logicquests.numInRow = int(row[0])
    logicquests.question = row[1]
    logicquests.question_img = row[2]
    logicquests.rightAnswer = row[3]
    logicquests.rightAnswerNum = int (row[4])
    logicquests.answerOne = row[5]
    logicquests.answerTwo = row[6]
    logicquests.answerThree = row[7]
    logicquests.answerFour = row[8]
    logicquests.answerFive = row[9]
    logicquests.answerSix = row[10]
    logicquests.answerSeven = row[11]
    logicquests.answerEight = row[12]
    logicquests.answerNine = row[13]
    logicquests.answerTen = row[14]
    logicquests.answerEleven = row[15]
    logicquests.answerTwelve = row[16]
    logicquests.save()

print(logic2.logicquest_set.all())