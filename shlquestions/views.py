from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect, Http404
from shlquestions.models import Shlquest, Shltest
import json
import random

def url_redirect(request):
    
    adverNum = str(random.randint(2,3))
    context = {
        'adverNum': adverNum,
       }


    return render(request, 'shlquestions/brief.html', context)


def shltest_view(request, shltest_id):
    try:
        shlTest1 = Shltest.objects.get(pk=shltest_id)
    except Shltest.DoesNotExist:
        raise Http404

    question = []
    image = []
    for i in range(1, shlTest1.shlquest_set.count()+1):
        quest1=shlTest1.shlquest_set.get(numInRow=i)
        question.append(quest1.question)
        image.append(str(quest1.question_img))
    qmlist =  zip(question, image)
    adverNum = str(random.randint(1,1))
    context = {
        'qmlist': qmlist,
        'shltest_id': shltest_id,
        'adverNum': adverNum,
       }
    return render(request, 'shlquestions/shl.html', context)

    #  return HttpResponse('<h1>we are in SHL '+str(shltest_id)+' test app</h1>')

def get_data(request, shltest_id):
    if request.is_ajax():
        try:
            shlTest1 = Shltest.objects.get(pk=shltest_id)
        except Shltest.DoesNotExist:
            raise Http404

        response_data = {}
        answers = []
        rightAnswers = []
        for i in range(1, shlTest1.shlquest_set.count()+1):
            quest1=shlTest1.shlquest_set.get(numInRow=i)
            temp = []
            temp.append(quest1.answerOne)
            temp.append(quest1.answerTwo)
            temp.append(quest1.answerThree)
            temp.append(quest1.answerFour)
            answers.append(temp)
            rightAnswers.append(quest1.rightAnswerNum)
       
        x = shlTest1.testCoverageStatistics
        statistics = x.split(",")
        statistics = list(map(int, statistics))
        
        response_data['answers'] = answers
        response_data['rightAnswers'] = rightAnswers
        response_data['statistics'] = statistics
        return HttpResponse(json.dumps(response_data), content_type='application/json')
    else:
        raise Http404

def post_data(request, shltest_id):
    if request.is_ajax():
        theNumber = int(request.POST.get("theNumber", ""))
        if (theNumber > 0):
            try:
                shlTest1 = Shltest.objects.get(pk=shltest_id)
            except:
                pass
            else:
                statistics = []
                x = shlTest1.testCoverageStatistics
                statistics = x.split(",")
                statistics = list(map(int, statistics))
                statistics[theNumber-1] = statistics[theNumber-1]+1
                newStatistics = ','.join(map(str, statistics)) 
                shlTest1.testCoverageStatistics = newStatistics
                shlTest1.save()
                return HttpResponse('')
        else:
            return HttpResponse('')
                #  x = shlTest1.testCoverageStatistics
                #  response_data = {}
                #  response_data['someNumber'] = x
                #  return HttpResponse(json.dumps(response_data), content_type='application/json')