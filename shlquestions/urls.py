from django.conf.urls import url
from . import views
from .models import Shltest


app_name = 'shlquestions'

urlpatterns = [
    #/shltest/
    url(r'^$', views.url_redirect, name='Shlredirect'),
    #/shltest/791/
    url(r'^shl/(?P<shltest_id>[0-9]+)/$', views.shltest_view, name='shltest_view'),
     #/shltest/1/get_data/
    url(r'^shl/(?P<shltest_id>[0-9]+)/get_data/$', views.get_data),
     #/shltest/1/post_data/
    url(r'^shl/(?P<shltest_id>[0-9]+)/post_data/$', views.post_data),

    
]

