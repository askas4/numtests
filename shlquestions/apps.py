from django.apps import AppConfig


class ShlquestionsConfig(AppConfig):
    name = 'shlquestions'
