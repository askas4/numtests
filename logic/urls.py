from django.conf.urls import url
from . import views
from .models import Logictest

app_name = 'logic'

urlpatterns = [
    #/tqtest/
    url(r'^$', views.url_redirect, name='logicredirect'),
    #/tqltest/791/
    url(r'^(?P<logictest_id>[0-9]+)/$', views.logictest_view, name='logictest_view'),
     #/tqltest/1/get_data/
    url(r'^(?P<logictest_id>[0-9]+)/get_data/$', views.get_data),
     #/tqltest/1/post_data/
    url(r'^(?P<logictest_id>[0-9]+)/post_data/$', views.post_data),
]

