from django.db import models

class Logictest(models.Model):
    testName = models.CharField(max_length=250)
    testCoverageStatistics = models.CharField(max_length=300)
    
    def __str__(self):
        return self.testName

class Logicquest(models.Model):
    LogicTest = models.ForeignKey(Logictest, on_delete=models.CASCADE,null=True)
    numInRow = models.IntegerField()
    question = models.CharField(max_length=1000)
    question_img = models.FileField(blank=True, null=True)
    rightAnswer = models.CharField(max_length=200)
    rightAnswerNum = models.IntegerField()
    answerOne = models.CharField(max_length=200)
    answerTwo = models.CharField(max_length=200)
    answerThree = models.CharField(max_length=200)
    answerFour = models.CharField(max_length=200)
    answerFive = models.CharField(max_length=200)
    answerSix = models.CharField(max_length=200)
    answerSeven = models.CharField(max_length=200)
    answerEight = models.CharField(max_length=200)
    answerNine = models.CharField(max_length=200)
    answerTen = models.CharField(max_length=200)
    answerEleven = models.CharField(max_length=200)
    answerTwelve = models.CharField(max_length=200)
    def __str__(self):
         return str(self.numInRow)+' - '+str(self.LogicTest)