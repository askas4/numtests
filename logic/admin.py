from django.contrib import admin

# Register your models here.
from .models import Logicquest, Logictest

admin.site.register(Logicquest)
admin.site.register(Logictest)