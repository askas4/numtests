from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect, Http404
from verbal.models import Verbalquest, Verbaltest
import json
import random

def url_redirect(request):
    
    adverNum = str(random.randint(2,3))
    context = {
        'adverNum': adverNum,
       }


    return render(request, 'verbal/brief.html', context)


def verbaltest_view(request, verbaltest_id):
    try:
        verbalTest1 = Verbaltest.objects.get(pk=verbaltest_id)
    except Verbaltest.DoesNotExist:
        raise Http404

    question = []
    text = []
    for i in range(1, verbalTest1.verbalquest_set.count()+1):
        quest1=verbalTest1.verbalquest_set.get(numInRow=i)
        question.append(quest1.question)
        text.append(quest1.question_txt)
    qmlist =  zip(question, text)
    adverNum = str(random.randint(1,1))
    context = {
        'qmlist': qmlist,
        'verbaltest_id': verbaltest_id,
        'adverNum': adverNum,
       }
    return render(request, 'verbal/verbal.html', context)

    #  return HttpResponse('<h1>we are in SHL '+str(shltest_id)+' test app</h1>')

def get_data(request, verbaltest_id):
    if request.is_ajax():
        try:
            verbalTest1 = Verbaltest.objects.get(pk=verbaltest_id)
        except Verbaltest.DoesNotExist:
            raise Http404

        response_data = {}
        answers = []
        rightAnswers = []
        for i in range(1, verbalTest1.verbalquest_set.count()+1):
            quest1=verbalTest1.verbalquest_set.get(numInRow=i)
            temp = []
            temp.append(quest1.answerOne)
            temp.append(quest1.answerTwo)
            temp.append(quest1.answerThree)
            answers.append(temp)
            rightAnswers.append(quest1.rightAnswerNum)
       
        x = verbalTest1.testCoverageStatistics
        statistics = x.split(",")
        statistics = list(map(int, statistics))
        
        response_data['answers'] = answers
        response_data['rightAnswers'] = rightAnswers
        response_data['statistics'] = statistics
        return HttpResponse(json.dumps(response_data), content_type='application/json')
    else:
        raise Http404

def post_data(request, verbaltest_id):
    if request.is_ajax():
        theNumber = int(request.POST.get("theNumber", ""))
        if (theNumber > 0):
            try:
                verbalTest1 = Verbaltest.objects.get(pk=verbaltest_id)
            except:
                pass
            else:
                statistics = []
                x = verbalTest1.testCoverageStatistics
                statistics = x.split(",")
                statistics = list(map(int, statistics))
                statistics[theNumber-1] = statistics[theNumber-1]+1
                newStatistics = ','.join(map(str, statistics)) 
                verbalTest1.testCoverageStatistics = newStatistics
                verbalTest1.save()
                return HttpResponse('')
        else:
            return HttpResponse('')
                #  x = shlTest1.testCoverageStatistics
                #  response_data = {}
                #  response_data['someNumber'] = x
                #  return HttpResponse(json.dumps(response_data), content_type='application/json')
