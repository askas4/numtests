from django.contrib import admin

from .models import Verbalquest, Verbaltest

admin.site.register(Verbaltest)
admin.site.register(Verbalquest)
