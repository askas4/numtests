from django.db import models

class Verbaltest(models.Model):
    testName = models.CharField(max_length=250)
    testCoverageStatistics = models.CharField(max_length=300)
    
    def __str__(self):
        return self.testName
        

class Verbalquest(models.Model):
    verbalTest = models.ForeignKey(Verbaltest, on_delete=models.CASCADE,null=True)
    numInRow = models.IntegerField()
    question = models.CharField(max_length=1000)
    question_txt = models.CharField(max_length=4000)
    rightAnswer = models.CharField(max_length=200)
    rightAnswerNum = models.IntegerField()
    answerOne = models.CharField(max_length=200)
    answerTwo = models.CharField(max_length=200)
    answerThree = models.CharField(max_length=200)
    def __str__(self):
         return str(self.numInRow)+' - '+str(self.verbalTest)