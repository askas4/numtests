from django.conf.urls import url
from . import views
from .models import Verbaltest

app_name = 'verbal'

urlpatterns = [
    #/shltest/
    url(r'^$', views.url_redirect, name='verbalredirect'),
    #/shltest/791/
    url(r'^(?P<verbaltest_id>[0-9]+)/$', views.verbaltest_view, name='verbaltest_view'),
     #/shltest/1/get_data/
    url(r'^(?P<verbaltest_id>[0-9]+)/get_data/$', views.get_data),
     #/shltest/1/post_data/
    url(r'^(?P<verbaltest_id>[0-9]+)/post_data/$', views.post_data),
]

