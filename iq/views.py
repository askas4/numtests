from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect, Http404
from iq.models import Iqquest, Iqtest
import json
import random

def url_redirect(request):
    
    adverNum = str(random.randint(2,3))
    context = {
        'adverNum': adverNum,
       }


    return render(request, 'iq/brief.html', context)


def iqtest_view(request, iqtest_id):
    try:
        iqTest1 = Iqtest.objects.get(pk=iqtest_id)
    except Iqtest.DoesNotExist:
        raise Http404

    question = []
    image = []
    for i in range(1, iqTest1.iqquest_set.count()+1):
        quest1=iqTest1.iqquest_set.get(numInRow=i)
        question.append(quest1.question)
        image.append(str(quest1.question_img))
    qmlist =  zip(question, image)
    adverNum = str(random.randint(1,1))
    context = {
        'qmlist': qmlist,
        'iqtest_id': iqtest_id,
        'adverNum': adverNum,
       }
    return render(request, 'iq/iq.html', context)

    #  return HttpResponse('<h1>we are in SHL '+str(shltest_id)+' test app</h1>')

def get_data(request, iqtest_id):
    if request.is_ajax():
        try:
            iqTest1 = Iqtest.objects.get(pk=iqtest_id)
        except Iqtest.DoesNotExist:
            raise Http404

        response_data = {}
        answers = []
        rightAnswers = []
        for i in range(1, iqTest1.iqquest_set.count()+1):
            quest1=iqTest1.iqquest_set.get(numInRow=i)
            temp = []
            temp.append(quest1.answerOne)
            temp.append(quest1.answerTwo)
            temp.append(quest1.answerThree)
            temp.append(quest1.answerFour)
            temp.append(quest1.answerFive)
            answers.append(temp)
            rightAnswers.append(quest1.rightAnswerNum)
       
        x = iqTest1.testCoverageStatistics
        statistics = x.split(",")
        statistics = list(map(int, statistics))
        
        response_data['answers'] = answers
        response_data['rightAnswers'] = rightAnswers
        response_data['statistics'] = statistics
        return HttpResponse(json.dumps(response_data), content_type='application/json')
    else:
        raise Http404

def post_data(request, iqtest_id):
    if request.is_ajax():
        theNumber = int(request.POST.get("theNumber", ""))
        if (theNumber > 0):
            try:
                iqTest1 = Iqtest.objects.get(pk=iqtest_id)
            except:
                pass
            else:
                statistics = []
                x = iqTest1.testCoverageStatistics
                statistics = x.split(",")
                statistics = list(map(int, statistics))
                statistics[theNumber-1] = statistics[theNumber-1]+1
                newStatistics = ','.join(map(str, statistics)) 
                iqTest1.testCoverageStatistics = newStatistics
                iqTest1.save()
                return HttpResponse('')
        else:
            return HttpResponse('')
                #  x = shlTest1.testCoverageStatistics
                #  response_data = {}
                #  response_data['someNumber'] = x
                #  return HttpResponse(json.dumps(response_data), content_type='application/json')
