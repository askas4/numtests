from django.contrib import admin

from .models import Iqquest, Iqtest

admin.site.register(Iqtest)
admin.site.register(Iqquest)
