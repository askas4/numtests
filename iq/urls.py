from django.conf.urls import url
from . import views
from .models import Iqtest

app_name = 'iq'

urlpatterns = [
    #/shltest/
    url(r'^$', views.url_redirect, name='iqredirect'),
    #/shltest/791/
    url(r'^(?P<iqtest_id>[0-9]+)/$', views.iqtest_view, name='iqtest_view'),
     #/shltest/1/get_data/
    url(r'^(?P<iqtest_id>[0-9]+)/get_data/$', views.get_data),
     #/shltest/1/post_data/
    url(r'^(?P<iqtest_id>[0-9]+)/post_data/$', views.post_data),
]

