
from django.contrib import admin
from django.urls import path, include 
from . import views

urlpatterns = [
    path('', include('home.urls')),
    path('admin/', admin.site.urls),
    path('chislovye-testy/', include('shlquestions.urls')),
    path('chislovye-testy/talent-q/', include('tqquestions.urls')),
    path('contact/', include('contact.urls')),
    path('logicheskiye-testy/', include('logic.urls')),
    path('inductive/', include('iq.urls')),
    path('verbalnyy-test-online/', include('verbal.urls')),

    
    path('numerical/', views.numredirect),
    path('verbal/', views.verbredirect),
    path('logical/', views.logicredirect),
    path('shl/', views.shlredirect),
    path('talent-q/', views.tqredirect),
    path('ontarget/', views.ontargetredirect),
    path('watson-glaser/', views.wgredirect),
    
]
