
from django.http import HttpResponse, HttpResponseRedirect, Http404




def numredirect(request):
    
  return HttpResponseRedirect("/chislovye-testy/")

def verbredirect(request):
    
  return HttpResponseRedirect("/verbalnyy-test-online/")

def logicredirect(request):
    
  return HttpResponseRedirect("/logicheskiye-testy/")


def shlredirect(request):
    
  return HttpResponseRedirect("/shl-testy/")

def tqredirect(request):
    
  return HttpResponseRedirect("/testy-talent-q/")

def ontargetredirect(request):
    
  return HttpResponseRedirect("/ontarget-testy/")

def wgredirect(request):
    
  return HttpResponseRedirect("/test-uotsona-gleyzera/")
